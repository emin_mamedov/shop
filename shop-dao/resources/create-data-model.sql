# DROP  DATABASE shop;
CREATE DATABASE IF NOT EXISTS shop;
USE shop;

CREATE TABLE IF NOT EXISTS manufacturer_tbl (
  id         INT         NOT NULL AUTO_INCREMENT,
  name       VARCHAR(20) NOT NULL,
  id_country INT         NOT NULL,
  id_address INT         NOT NULL,
  UNIQUE UQ_NAME_ORG (name),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS supplier_tbl (
  id           INT         NOT NULL AUTO_INCREMENT,
  name         VARCHAR(20) NOT NULL,
  id_address   INT         NOT NULL,
  id_telephone INT         NOT NULL,
  id_email     INT         NOT NULL,
  UNIQUE UQ_NAME_ORG (name),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS telephone_tbl (
  id     INT         NOT NULL AUTO_INCREMENT,
  number VARCHAR(12) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS country_tbl (
  id   INT         NOT NULL AUTO_INCREMENT,
  name VARCHAR(30) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS address_tbl (
  id      INT         NOT NULL AUTO_INCREMENT,
  address VARCHAR(40) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS category_tbl (
  id          INT         NOT NULL AUTO_INCREMENT,
  name        VARCHAR(20) NOT NULL,
  id_category INT         NULL,
  UNIQUE UQ_NAME_ORG (name),
  FOREIGN KEY (id_category) REFERENCES category_tbl (id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS feature_tbl (
  id   INT         NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  UNIQUE UQ_NAME_ORG (name),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS product_tbl (
  id          INT         NOT NULL AUTO_INCREMENT,
  name        VARCHAR(20) NOT NULL,
  id_category INT         NOT NULL,
  id_feature  INT         NOT NULL,
  comment     VARCHAR(40) NULL,
  UNIQUE UQ_NAME_ORG (name),
  FOREIGN KEY (id_category) REFERENCES category_tbl (id),
  FOREIGN KEY (id_feature) REFERENCES feature_tbl (id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS manufacturer_product_tbl (
  id              INT NOT NULL AUTO_INCREMENT,
  id_product      INT NOT NULL,
  id_manufacturer INT NOT NULL,
  FOREIGN KEY (id_product) REFERENCES product_tbl (id),
  FOREIGN KEY (id_manufacturer) REFERENCES manufacturer_tbl (id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS product_price_tbl (
  id            INT  NOT NULL AUTO_INCREMENT,
  id_product    INT  NOT NULL,
  price         INT  NOT NULL,
  count         INT  NOT NULL,
  date_delivery DATE NOT NULL,
  id_supplier   INT  NOT NULL,
  FOREIGN KEY (id_product) REFERENCES product_tbl (id),
  FOREIGN KEY (id_supplier) REFERENCES supplier_tbl (id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS client_tbl (
  id           INT         NOT NULL AUTO_INCREMENT,
  surname      VARCHAR(20) NOT NULL,
  name         VARCHAR(15) NOT NULL,
  family_name  VARCHAR(20) NOT NULL,
  id_email     INT         NOT NULL,
  id_telephone INT         NOT NULL,
  id_address   INT         NOT NULL,
  login        VARCHAR(20) NOT NULL,
  password     VARCHAR(20) NOT NULL,
  id_group     INT         NOT NULL,
  UNIQUE UQ_NAME_ORG (login),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS email_tbl (
  id    INT         NOT NULL AUTO_INCREMENT,
  email VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS group_tbl (
  id      INT         NOT NULL AUTO_INCREMENT,
  `group` VARCHAR(20) NOT NULL,
  UNIQUE UQ_NAME_ORG (`group`),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS news_tbl (
  id              INT  NOT NULL AUTO_INCREMENT,
  date_publishing DATE NOT NULL,
  date_end        DATE NOT NULL,
  text            VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS comment_tbl (
  id          INT     NOT NULL AUTO_INCREMENT,
  id_product  INT     NOT NULL,
  id_client   INT     NOT NULL,
  rating      TINYINT NOT NULL,
  date_create DATE    NOT NULL,
  text        VARCHAR(60),
  FOREIGN KEY (id_product) REFERENCES product_tbl (id),
  FOREIGN KEY (id_client) REFERENCES client_tbl (id),
  PRIMARY KEY (id)
);

ALTER TABLE manufacturer_tbl ADD FOREIGN KEY (id_country) REFERENCES country_tbl (id);

ALTER TABLE manufacturer_tbl ADD FOREIGN KEY (id_address) REFERENCES address_tbl (id);

ALTER TABLE supplier_tbl ADD FOREIGN KEY (id_address) REFERENCES address_tbl (id);

ALTER TABLE supplier_tbl ADD FOREIGN KEY (id_telephone) REFERENCES telephone_tbl (id);

ALTER TABLE supplier_tbl ADD FOREIGN KEY (id_email) REFERENCES email_tbl (id);

ALTER TABLE client_tbl ADD FOREIGN KEY (id_email) REFERENCES email_tbl (id);

ALTER TABLE client_tbl ADD FOREIGN KEY (id_telephone) REFERENCES telephone_tbl (id);

ALTER TABLE client_tbl ADD FOREIGN KEY (id_address) REFERENCES address_tbl (id);

ALTER TABLE client_tbl ADD FOREIGN KEY (id_group) REFERENCES group_tbl (id);
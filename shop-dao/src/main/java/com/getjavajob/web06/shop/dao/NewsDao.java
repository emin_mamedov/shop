package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.News;

import java.sql.*;

public class NewsDao extends GenericDao<News> {

    private static final String TABLE_NAME = "news_tbl";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME + " (name) VALUES (?)";
    private static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET name = ? WHERE id = ?";

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE_BY_ID;
    }

    @Override
    protected News createInstanceFromResult(ResultSet resultSet) throws SQLException {
        News news = new News();
        news.setId(resultSet.getInt("id"));
        news.setPublicationDate(resultSet.getDate("date_publishing"));
        news.setEndDate(resultSet.getDate("date_end"));
        news.setText(resultSet.getString("text"));
        return news;
    }
}

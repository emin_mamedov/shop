package com.getjavajob.web06.shop.dao.impl;

import com.getjavajob.web06.shop.dao.ConnectionPool;
import com.getjavajob.web06.shop.dao.ProductDao;
import com.getjavajob.web06.shop.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Mikhail Nemenko on 08.11.2015.
 * <p>
 * Реализация Дао для работы с сущностью продукт
 *
 * @author Mikhail Nemenko
 * @version 1.0
 */
public class ProductDaoJDBCImpl extends ProductDao {
    private static final String PRODUCTS_BY_MANUFACTURER = "SELECT b.* FROM manufacturer_product_tbl AS a" +
            " INNER JOIN product_tbl AS b ON a.id_product = b.ID WHERE ID_MANUFACTURER = ? ";
    private static final String PRODUCTS_BY_CATEGORY = "SELECT * FROM product_tbl WHERE id_category = ?";
    private static final String PRODUCTS_BY_FEATURES = "SELECT b.* FROM features_product_tbl AS a" +
            " INNER JOIN product_tbl AS b ON a.id_product = b.id WHERE a.id_feature IN( ? )";

    @Override
    protected String getTableName() {
        return "product_tbl";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + getTableName() + "(id, name, id_category, id_feature, comment) VALUES (?, ?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateByIdStatement() {
        return "UPDATE " + getTableName() + " SET id = ?, name = ?, id_category = ?, id_feature = ?, comment = ? WHERE id = ?";
    }


    @Override
    protected Product createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        while (resultSet.next()) {
            product.setName(resultSet.getString("name"));
            product.setCategory((Category) resultSet.getObject("id_category"));
            //тут скорее всего волоса  потому что связи в базе неверные надо глядеть
            product.setFeatures((List<Feature>) resultSet.getObject("id_feature"));
            product.setManufacturer((Manufacturer) resultSet.getObject("id_manufacturer"));
            product.setComments((List<Comment>) resultSet.getObject("id_comment"));
        }
        return product;
    }

    @Override
    public List<Product> getProductsByManufacturer(Manufacturer manufacturer) {
        Connection connection = null;
        List<Product> products = new ArrayList<>();
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(PRODUCTS_BY_MANUFACTURER)) {
                prepareStatement.setInt(1, manufacturer.getId());
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    while (resultSet.next()) {
                        products.add(createInstanceFromResult(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
        return products;
    }

    @Override
    public List<Product> getProductsByCategory(Category category) {
        Connection connection = null;
        List<Product> products = new ArrayList<>();
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(PRODUCTS_BY_CATEGORY)) {
                prepareStatement.setInt(1, category.getId());
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    while (resultSet.next()) {
                        products.add(createInstanceFromResult(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
        return products;
    }

    @Override
    public Set<Product> getProductsByFeatures(List<Feature> features) {
        Set<Product> products = new HashSet<>();
        StringBuffer sb = new StringBuffer(PRODUCTS_BY_FEATURES);
        for (Feature feature : features) {
            sb.insert(PRODUCTS_BY_FEATURES.lastIndexOf(")") - 1, ", ?");
        }
        Connection connection = null;
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(sb.toString())) {
                for (int i = 0; i < features.size(); i++) {
                    prepareStatement.setInt(i + 1, features.get(i).getId());
                }
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    while (resultSet.next()) {
                        products.add(createInstanceFromResult(resultSet));
                    }
                }
            } catch (SQLException e) {
                return Collections.emptySet();
            }
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
        return products;
    }
}

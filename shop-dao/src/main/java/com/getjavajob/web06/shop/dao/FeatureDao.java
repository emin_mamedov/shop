package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.Feature;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mkorn on 08.11.2015.
 */
public class FeatureDao extends GenericDao<Feature> {

    private static final String TABLE_NAME = "comment_tbl";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME + " (name) VALUES (?)";
    private static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET name = ? WHERE id = ?";

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE_BY_ID;
    }

    @Override
    protected Feature createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Feature feature = new Feature();
        feature.setId(resultSet.getInt("id"));
        feature.setName(resultSet.getString("name"));
        return feature;
    }
}

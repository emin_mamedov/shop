package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.Manufacturer;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by ����� on 08.11.2015.
 */
public class ManufacturerDao extends GenericDao<Manufacturer> {

    @Override
    protected String getTableName() {
        return "manufacture_tbl";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + getTableName() + " (name,country,address) VALUES (?,?,?)";
    }

    @Override
    protected String getUpdateByIdStatement() {
        return "UPDATE " + getTableName() + " SET name = ?,country=?,address=? WHERE id = ?";
    }

    @Override
    protected Manufacturer createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(resultSet.getInt("id"));
        manufacturer.setName(resultSet.getString("name"));
        manufacturer.setCountry(resultSet.getString("country"));
        manufacturer.setAddress(resultSet.getString("address"));
        return manufacturer;
    }
}

package com.getjavajob.web06.shop.dao;


import com.getjavajob.web06.shop.model.Manufacturer;
import com.getjavajob.web06.shop.model.ProductPrice;
import com.getjavajob.web06.shop.model.Supplier;

import java.sql.Date;
import java.util.List;

public abstract class ProductPriceDao extends GenericDao<ProductPrice>{

    public abstract List<ProductPrice> getProductPriceBySupplier(Supplier supplier);

    public abstract List<ProductPrice> getProductPriceByDateOfDelivery(Date dateOfDelivery);
}

package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.BaseEntity;

import java.util.List;

public interface CrudDao<T extends BaseEntity> {

	void add(T entity);
	
	void update(T entity);
	
	void delete(T entity);
	
	T get(int id);
	
	List<T> getAll();

}

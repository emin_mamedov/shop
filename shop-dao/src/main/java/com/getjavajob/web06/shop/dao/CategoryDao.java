package com.getjavajob.web06.shop.dao;


import com.getjavajob.web06.shop.model.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDao extends GenericDao {


    @Override
    protected String getTableName() {
        return "category_tbl";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + getTableName() + "(id, name, id_category) VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateByIdStatement() {
        return "UPDATE " + getTableName() + "SET id = ?, name = ?, id_category = ? WHERE id =?";
    }

    @Override
    protected Category createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Category category = new Category();
        category.setId(Integer.parseInt(resultSet.getString("id")));
        category.setName(resultSet.getString("name"));
        String parentCategory = resultSet.getString("id_category");
        if (parentCategory != null) {
            category.setParentCategory((Category) new CategoryDao().get(Integer.parseInt(parentCategory)));
        }
        return category;
    }
}

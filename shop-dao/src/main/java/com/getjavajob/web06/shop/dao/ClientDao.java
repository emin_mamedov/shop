package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.Client;
import com.getjavajob.web06.shop.model.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDao extends GenericDao<Client> {
        private static final String SELECT_RELATIONS = "SELECT " +
            "`CLIENT_TBL`.`ID`," +
            "`GROUP_TBL`.`GROUP` AS `group` " +
            "FROM `CLIENT_TBL` " +
            "INNER JOIN `GROUP_TBL` ON `CLIENT_TBL`.`ID_GROUP`=`GROUP_TBL`.`ID` " +
            "WHERE `CLIENT_TBL`.`ID`=?";

    @Override
    protected String getTableName() {
        return "CLIENT_TBL";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + getTableName() + " (SURNAME,NAME,FAMILY_NAME,EMAIL,TELEPHONE,ADDRESS,LOGIN,PASSWORD,ID_GROUP) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateByIdStatement() {
        return "UPDATE " + getTableName() + " SET SURNAME=?,NAME=?,FAMILY_NAME=?,EMAIL=?,TELEPHONE=?,ADDRESS=?,LOGIN=?,PASSWORD=?,ID_GROUP=? WHERE id =?";
    }

    @Override
    protected Client createInstanceFromResult(ResultSet rs) throws SQLException {
        int clientId = rs.getInt("id");
        Client client = new Client(clientId);

        client.setFirstName(rs.getString("NAME"));
        client.setLastName(rs.getString("SURNAME"));
        client.setPatronymic(rs.getString("FAMILY_NAME"));
        client.setLogin(rs.getString("LOGIN"));
        client.setPassword(rs.getString("PASSWORD"));
        client.setAddress(rs.getString("ADDRESS"));
        client.setEmail(rs.getString("EMAIL"));
        client.setPhone(rs.getString("TELEPHONE"));

        setRelations(clientId, client);
        return client;
    }

    private void setRelations(int id, Client client) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(SELECT_RELATIONS)) {
                prepareStatement.setInt(1, id);
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    if (resultSet.next()) {
                        client.setGroup(Group.valueOf(getEntity(resultSet, "group")));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
    }

    private String getEntity(ResultSet rs, String field) throws SQLException {
        String entity = null;
        if (rs!=null) {
            entity = rs.getString(field);
        }
        return entity;
    }
}

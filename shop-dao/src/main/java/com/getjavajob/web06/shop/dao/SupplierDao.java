package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.Supplier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by levgubin on 08.11.15.
 */

public class SupplierDao extends GenericDao<Supplier> {

    private static final String TABLE_NAME = "supplier_tbl";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (name, address, phone, email) VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME
            + " SET name = ?, address = ?, email = ?, telephone = ?";

    private static final String SELECT_ALL = "SELECT * FROM supplier";
    private static final String SELECT_BY_NAME = SELECT_ALL + " WHERE name=?";

    public Supplier getByName(String name) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME)) {
                preparedStatement.setString(1, name);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return createSupplierFromResult(resultSet);
                    }
                }
                return null;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
    }

    private Supplier createSupplierFromResult(ResultSet resultSet)
            throws SQLException {
        Supplier supplier = new Supplier();
        supplier.setId(resultSet.getInt("id"));
        supplier.setName("name");
        supplier.setAddress("address");
        supplier.setEmail("email");
        supplier.setPhoneNumber("telephone");
        return supplier;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE;
    }

    @Override
    protected Supplier createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Supplier supplier = new Supplier();
        supplier.setId(resultSet.getInt("id"));
        supplier.setPhoneNumber(resultSet.getString("telephone"));
        supplier.setAddress(resultSet.getString("address"));
        supplier.setEmail(resultSet.getString("email"));
        return supplier;
    }

}

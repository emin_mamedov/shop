package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.dao.impl.ProductDaoJDBCImpl;
import com.getjavajob.web06.shop.model.Client;
import com.getjavajob.web06.shop.model.Comment;
import com.getjavajob.web06.shop.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mkorn on 08.11.2015.
 */
public class CommentDao extends GenericDao<Comment> {

    private static final String TABLE_NAME = "comment_tbl";
    private static final String INSERT =
            "INSERT INTO " + TABLE_NAME +
                    " (id_product, id_client, rating, date_create, text) " +
                    "VALUES " +
                    "(?, ?, ?, ?, ?)";
    private static final String UPDATE_BY_ID =
            "UPDATE " + TABLE_NAME +
                    " SET " +
                    "id_product = ?, " +
                    "id_client = ?, " +
                    "rating = ?, " +
                    "date_create = ?, " +
                    "text = ? " +
                    "WHERE id = ?";

    private static final String SELECT_BY_PRODUCT = "SELECT * FROM " + TABLE_NAME + " WHERE id_product = ?";
    private static final String SELECT_BY_CLIENT = "SELECT * FROM " + TABLE_NAME + " WHERE id_client = ?";

    public List<Comment> getByProduct(Product product) {
        List<Comment> comments = new ArrayList<>();
        Connection connection = null;
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_PRODUCT)) {
                int productId = product.getId();
                preparedStatement.setInt(1, productId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        comments.add(createInstanceFromResult(resultSet));
                    }
                    return comments;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
    }

    public List<Comment> getByClient(Client client) {
        List<Comment> comments = new ArrayList<>();
        Connection connection = null;
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_CLIENT)) {
                int clientId = client.getId();
                preparedStatement.setInt(1, clientId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        comments.add(createInstanceFromResult(resultSet));
                    }
                    return comments;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE_BY_ID;
    }

    @Override
    protected Comment createInstanceFromResult(ResultSet resultSet) throws SQLException {
        Comment comment = new Comment();
        comment.setId(resultSet.getInt("id"));
        ProductDao productDao = new ProductDaoJDBCImpl(); // tbd
        Product product = productDao.get(resultSet.getInt("id_product"));
        comment.setProduct(product);
        ClientDao clientDao = new ClientDao(); // tbd
        Client client = clientDao.get(resultSet.getInt("id_client"));
        comment.setClient(client);
        comment.setRating(resultSet.getInt("rating"));
        comment.setCreateDate(resultSet.getDate("date_create"));
        comment.setText(resultSet.getString("text"));
        return comment;
    }
}

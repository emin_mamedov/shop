package com.getjavajob.web06.shop.dao;

import com.getjavajob.web06.shop.model.Category;
import com.getjavajob.web06.shop.model.Feature;
import com.getjavajob.web06.shop.model.Manufacturer;
import com.getjavajob.web06.shop.model.Product;

import java.util.List;
import java.util.Set;

/**
 * Created by Mikhail Nemenko on 08.11.2015.
 *
 * Дао для работы с сущностью продукта
 *
 * @version 1.0
 * @author Mikhail Nemenko
 */

public abstract class ProductDao extends GenericDao<Product> {

    public abstract List<Product> getProductsByManufacturer(Manufacturer manufacturer);

    public abstract List<Product> getProductsByCategory(Category category);

    public abstract Set<Product> getProductsByFeatures(List<Feature> features);
}

package com.getjavajob.web06.shop.dao.impl;

import com.getjavajob.web06.shop.dao.ConnectionPool;
import com.getjavajob.web06.shop.dao.ProductPriceDao;
import com.getjavajob.web06.shop.model.Manufacturer;
import com.getjavajob.web06.shop.model.Product;
import com.getjavajob.web06.shop.model.ProductPrice;
import com.getjavajob.web06.shop.model.Supplier;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementation DAO layer for ProductPrice entity
 *
 * @author Nesterenko Maxim
 * @version 1.0
 */
public class ProductPriceDaoJDBCImpl extends ProductPriceDao {
    private static final String NAME_TABLE = "product_price_tbl";
    private static final String INSERT = "INSERT INTO " + NAME_TABLE + "(id, id_product, price, count, date_delivery, id_supplier)" +
            " VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE " + NAME_TABLE +
            " SET id_product = ?, price = ?, count = ?, date_delivery = ?, id_supplier = ?" +
            " WHERE id = ?";
    private static final String PRODUCTS_PRICE_BY_SUPPLIER = "SELECT P.name, PP.price, PP.count " +
            "FROM Product_Price_tbl AS PP " +
            "INNER JOIN Product_tbl AS P " +
            "ON PP.id_product = P.id " +
            "WHERE PP.id_supplier = ?";
    private static final String PRODUCT_PRICE_BY_DATE_DELIVERY = "SELECT P.name, PP.price, PP.count " +
            "FROM Product_Price_tbl AS PP " +
            "INNER JOIN Product_tbl AS P " +
            "ON PP.id_product = P.id " +
            "WHERE PP.date_delivery = ?";

    @Override
    protected String getTableName() {
        return NAME_TABLE;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE;
    }

    @Override
    protected ProductPrice createInstanceFromResult(ResultSet resultSet) throws SQLException {
        ProductPrice productPrice = new ProductPrice();
        while (resultSet.next()) {
            productPrice.setProduct((Product) resultSet.getObject("id_product"));
            productPrice.setPrice(resultSet.getInt("price"));
            productPrice.setCount(resultSet.getInt("count"));
            productPrice.setDateOfDelivery(resultSet.getDate("data_delivery"));
            productPrice.setManufacturer((Manufacturer) resultSet.getObject("id_supplier"));
        }
        return productPrice;
    }

    @Override
    public List<ProductPrice> getProductPriceBySupplier(Supplier supplier) {
        Connection connection = null;
        List<ProductPrice> productPrices = new ArrayList<>();
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(PRODUCTS_PRICE_BY_SUPPLIER)) {
                prepareStatement.setInt(1, supplier.getId());
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    while (resultSet.next()) {
                        productPrices.add(createInstanceFromResult(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
        return productPrices;
    }

    @Override
    public List<ProductPrice> getProductPriceByDateOfDelivery(Date dateOfDelivery) {
        Connection connection = null;
        List<ProductPrice> productPrices = new ArrayList<>();
        try {
            connection = ConnectionPool.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(PRODUCT_PRICE_BY_DATE_DELIVERY)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        productPrices.add(createInstanceFromResult(resultSet));
                    }
                }
            }
                        ConnectionPool.releaseConnection(connection);

        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            ConnectionPool.releaseConnection(connection);
        }
        return productPrices;
    }
}

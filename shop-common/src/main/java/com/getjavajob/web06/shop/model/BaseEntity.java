package com.getjavajob.web06.shop.model;

/**
 * Created by Nat on 08.11.2015.
 */
public abstract class BaseEntity {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BaseEntity(int id) {
        this.id = id;
    }

    public BaseEntity() {
    }
}

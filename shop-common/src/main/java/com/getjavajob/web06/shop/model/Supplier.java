package com.getjavajob.web06.shop.model;

public class Supplier extends BaseEntity {
    private String name;
    private String address;
    private String phoneNumber;
    private String email;

    public Supplier() {
    }

    public Supplier(int id) {
        super(id);
    }

    public Supplier(int id, String name, String address, String phoneNumber, String email) {
        setId(id);
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Supplier supplier = (Supplier) o;
        return name != null ? name.equals(supplier.getName()) : false;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Supplier: " + name;
    }
}

package com.getjavajob.web06.shop.model;

import java.sql.Date;

public class ProductPrice extends BaseEntity{
    private Product product;
    private Integer price;
    private Integer count;
    private Date dateOfDelivery;
    private Manufacturer manufacturer;

    public ProductPrice(){
    }

    public ProductPrice(int id) {
        super(id);
    }

    public ProductPrice(int id, Product product, Integer price, Integer count, Date dateOfDelivery, Manufacturer manufacturer) {
        super(id);
        this.product = product;
        this.price = price;
        this.count = count;
        this.dateOfDelivery = dateOfDelivery;
        this.manufacturer = manufacturer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(Date dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "Product: " + product +
                " Price: "  + price +
                " Count: " + count +
                " DateOfDelivery: " + dateOfDelivery +
                " Manufacturer: " + manufacturer;
    }
}

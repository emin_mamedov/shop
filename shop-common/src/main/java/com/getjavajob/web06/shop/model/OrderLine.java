package com.getjavajob.web06.shop.model;

public class OrderLine extends BaseEntity {
    private Product product;
    private ProductPrice productPrice;
    private int quantity;

    public OrderLine(int id, Product product, ProductPrice productPrice, int quantity) {
        super(id);
        this.product = product;
        this.productPrice = productPrice;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
package com.getjavajob.web06.shop.model;

public class Feature extends BaseEntity {
    private String name;

    public Feature() {
    }

    public Feature(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

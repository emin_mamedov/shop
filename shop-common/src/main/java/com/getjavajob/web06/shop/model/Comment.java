package com.getjavajob.web06.shop.model;

import java.sql.Date;

public class Comment extends BaseEntity {
    private Product product;
    private Client client;
    private int rating;
    private Date createDate;
    private String text;

    public Comment() {
    }

    public Comment(int id, Client client, Product product, int rating, Date createDate, String text) {
        super(id);
        this.client = client;
        this.product = product;
        this.rating = rating;
        this.createDate = createDate;
        this.text = text;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package com.getjavajob.web06.shop.model;

import java.util.List;

/**
 * Модель сущности Продукт
 *
 * @author Mikhail Nemenko
 * @version 1.0
 */
public class Product extends BaseEntity {
    private String name;
    private Category category;
    private List<Feature> features;
    private Manufacturer manufacturer;
    private List<Comment> comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Product(int id, String name, Category category, List<Feature> features, Manufacturer manufacturer, List<Comment> comments) {
        super(id);
        this.name = name;
        this.category = category;
        this.features = features;
        this.manufacturer = manufacturer;
        this.comments = comments;
    }

    public Product(int id) {
        super(id);
    }

    public Product() {
    }
}

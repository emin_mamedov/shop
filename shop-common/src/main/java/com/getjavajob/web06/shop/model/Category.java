package com.getjavajob.web06.shop.model;

public class Category extends BaseEntity {
    private String name;
    private Category parentCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return !(name != null ? !name.equals(category.name) : category.name != null) && !(parentCategory != null ?
                !parentCategory.equals(category.parentCategory) : category.parentCategory != null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (parentCategory != null ? parentCategory.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Name: " + name + " Parent: " + parentCategory;
    }
}

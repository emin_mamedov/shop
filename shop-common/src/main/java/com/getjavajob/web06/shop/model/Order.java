package com.getjavajob.web06.shop.model;

public class Order extends BaseEntity {
    private OrderLine orderLine;
    private Client client;

    public Order(int id, OrderLine orderLine, Client client) {
        super(id);
        this.orderLine = orderLine;
        this.client = client;
    }

    public OrderLine getOrderLine() {
        return orderLine;
    }

    public void setOrderLine(OrderLine orderLine) {
        this.orderLine = orderLine;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
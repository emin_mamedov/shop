package com.getjavajob.web06.shop.ui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

public interface GenericDao {

	public Employee get(int id);

	public void delete(int id);

	public List<Employee> getAll();

	public void update(Employee employee);
	
}

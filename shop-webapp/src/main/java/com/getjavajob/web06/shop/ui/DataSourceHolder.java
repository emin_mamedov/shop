package com.getjavajob.web06.shop.ui;

import javax.sql.DataSource;

public class DataSourceHolder {

	private static DataSource dataSource;

	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		DataSourceHolder.dataSource = dataSource;
	}

}

package com.getjavajob.web06.shop.ui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class EmployeeDao implements GenericDao {

    private static final String SELECT_ALL = "SELECT * FROM employee";
    private static final String SELECT_BY_ID = SELECT_ALL + " WHERE id=?";
    private static final String UPDATE_BY_ID = "UPDATE employee SET name=?, surname=?, phone=? WHERE id=?";
    private static final String DELETE_BY_ID = "DELETE FROM employee WHERE id=?";

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    public EmployeeDao(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public Employee get(int id) {
        return this.jdbcTemplate.queryForObject(SELECT_BY_ID, new Object[] { id }, new RowMapper<Employee>() {
            @Override
            public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
                return createEmployeeFromResult(rs);
            }
        });
    }

    @Override
    public void delete(int id) {
        try (Connection connection = this.dataSource.getConnection()) {
            try (PreparedStatement prepareStatement = connection.prepareStatement(DELETE_BY_ID)) {
                prepareStatement.setInt(1, id);
                prepareStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        } catch (SQLException e2) {
            e2.printStackTrace();
        }
    }

    @Override
    public List<Employee> getAll() {
        EntityManager em = entityManagerFactory.createEntityManager();
        try {
            em.getTransaction().begin();
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
            Root<Employee> from = criteriaQuery.from(Employee.class);
            CriteriaQuery<Employee> select = criteriaQuery.select(from);
            List<Employee> emps = em.createQuery(select).getResultList();
            em.getTransaction().commit();
            return emps;
        } finally {
            // if (em.getTransaction().isActive()) {
            // em.getTransaction().rollback();
            // }
        }
    }

    @Override
    @Transactional
    public void update(Employee employee) {
        this.jdbcTemplate.update(UPDATE_BY_ID, employee.getName(), employee.getSurname(), employee.getPhone(),
                employee.getId());
    }

    private Employee createEmployeeFromResult(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getInt("id"));
        employee.setName(resultSet.getString("name"));
        employee.setSurname(resultSet.getString("surname"));
        employee.setPhone(resultSet.getString("phone"));
        return employee;
    }

}

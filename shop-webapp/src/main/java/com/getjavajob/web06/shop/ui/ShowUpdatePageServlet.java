package com.getjavajob.web06.shop.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ShowUpdatePageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private EmployeeDao dao;
	
	@Override
	public void init() throws ServletException {
		WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		this.dao = applicationContext.getBean(EmployeeDao.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		Employee employee = dao.get(Integer.valueOf(req.getParameter("id")));
		req.setAttribute("employee", employee);
		req.getRequestDispatcher("/WEB-INF/jsp/updateEmployee.jsp").forward(req, resp);
	}

}

package com.getjavajob.web06.shop.ui;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private static final Logger perfLogger = LoggerFactory.getLogger("PerformanceLogger");
    private static final Logger rolLogger = LoggerFactory.getLogger("RollingLogger");

    @Autowired
    private GenericDao dao;

    @RequestMapping("/showEmployees")
    public ModelAndView showEmployees() {
        logger.debug("I'm in showEmployees method");
        List<Employee> emps = dao.getAll();
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("employees", emps);
        logger.info("end of showEmployees method");
        perfLogger.debug("Method executed in " + 10 + "ms");
        return modelAndView;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView showUpdate(@RequestParam("id") int id) {
        logger.debug("Going to show update page for " + id);
        // logger.debug("Going to show update %s %s page %s for", 1, 5, id);
        Employee employee = dao.get(id);
        ModelAndView modelAndView = new ModelAndView("updateEmployee");
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }

    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
    public String doUpdate(@ModelAttribute Employee employee) {
        System.out.println(employee);
        dao.update(employee);
        return "redirect:/showEmployees";
    }

    @RequestMapping(value = "/employee/{id}/view", method = RequestMethod.GET)
    public ModelAndView view(@PathVariable("id") int id) {
        return showUpdate(id);
    }

    @RequestMapping("/getEmployees")
    @ResponseBody
    public List<Employee> getEmployees(final @RequestParam("filter") String filter, HttpServletResponse response) {
        List<Employee> emps = dao.getAll();
        CollectionUtils.filter(emps, new Predicate<Employee>() {

            @Override
            public boolean evaluate(Employee employee) {
                return employee.getSurname().contains(filter) || employee.getName().contains(filter);
            }
        });
        return emps;
    }

}

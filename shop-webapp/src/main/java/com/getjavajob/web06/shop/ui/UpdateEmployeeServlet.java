package com.getjavajob.web06.shop.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class UpdateEmployeeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private EmployeeDao dao;
	
	@Override
	public void init() throws ServletException {
		WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		this.dao = applicationContext.getBean(EmployeeDao.class);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		Employee employee = new Employee();
		employee.setId(Integer.valueOf(req.getParameter("id")));
		employee.setName(req.getParameter("name"));
		employee.setSurname(req.getParameter("surname"));
		employee.setPhone(req.getParameter("phone"));
		dao.update(employee);
		resp.sendRedirect(req.getContextPath() + "/hello");
	}

}

package com.getjavajob.web06.shop.ui;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

	private AtomicInteger sessionCount;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("Session count " + this.sessionCount.incrementAndGet());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
	}

}

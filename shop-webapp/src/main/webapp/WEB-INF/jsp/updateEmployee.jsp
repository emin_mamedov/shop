<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Update Employee</title>
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/doUpdate" method="post">
			<input type="hidden" name="id" value="${employee.id}" />
			<input type="text" name="name" value="${employee.name}" />
			<input type="text" name="surname" value="${employee.surname}" />
			<input type="text" name="phone" value="${employee.phone}" />
			<input type="submit" name="Save" value="Save" />
		</form>
	</body>
</html>
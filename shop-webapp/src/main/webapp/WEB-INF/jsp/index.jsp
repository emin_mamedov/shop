<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Test</title>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	</head>
	<body>
		<!-- HMTL comment -->
		<%-- JSP Comment - Secret. No one should see it --%>
		<table>
			<c:forEach var="employee" items="${employees}">
				<tr>
					<td><a href='<c:url value="/update?id=${employee.id}" />'>${employee.name}</a></td>
					<td>${employee.phone}</td>
				</tr>
			</c:forEach>
		</table>
		<div class="ui-widget">
			<input id="employeeName" />
			<input id="button" type="button" />
		</div>
		<script>
		$(document).ready(function() {
		    $("input[type='button']").click(function() {
		        var filter = $("#employeeName").val();
		        $.get('<c:url value="/getEmployees?filter=" />' + filter, function(data) {
	                alert(JSON.stringify($.map( data, function( employee, i ) {
	                   return {value : employee.name, label : employee.name + ' ' + employee.surname }
	                })));
	                // var obj = JSON.parse("{filter: 'myfilter'}");
	                // obj.filter == 'myfilter';
		    	});
		    });
		    
		    
		    
		    
		    $( "#employeeName" ).autocomplete({
		        source: function( request, response ) {
		            /* $.ajax({
		              url: '<c:url value="/getEmployees" />',
		              data: {
		                filter: request.term
		              },
		              method: "GET", 
		              success: function( data ) {
		                response($.map( data, function( employee, i ) {
		                   return {value : employee.name, label : employee.name + ' ' + employee.surname }
		                }));
		              },
		              error: function() {
		                  console.log('Error');
		              }
		            }); */
		            
		            /* $.get('<c:url value="/getEmployees?filter=" />' + request.term, function(data) {
		                response($.map( data, function( employee, i ) {
		                   return {value : employee.name, label : employee.name + ' ' + employee.surname }
		                }));
		            }); */
		            
		            $.post('<c:url value="/getEmployees" />', {filter : request.term}, function(data) {
		                response($.map( data, function( employee, i ) {
			                   return {value : employee.name, label : employee.name + ' ' + employee.surname }
			                }));
			            });
		          },
		        minLength: 2
		      });
		    }
		);
	  </script>
 	</body>
</html>
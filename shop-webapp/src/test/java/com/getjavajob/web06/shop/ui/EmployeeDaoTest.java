package com.getjavajob.web06.shop.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.gjt.mm.mysql.Driver;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:shop-context.xml",
		"classpath:shop-context-overrides.xml" })
public class EmployeeDaoTest {

	@Autowired
	private GenericDao dao;
	@Value("#{T(${database.driver}).DBNAME_PROPERTY_KEY}")
	private List<String> username;

	@Test
	public void getById_Exist() {
		Employee employee = this.dao.get(2);

		assertEquals("Petr", employee.getName());
	}

	@Test
	public void getAll_OK() {
		List<Employee> employees = this.dao.getAll();

		assertEquals(4, employees.size());
		System.out.println(employees);
	}

	@Test
	@Ignore
	public void delete_OK() {
		this.dao.delete(1);

		Employee employee = this.dao.get(1);

		assertNull(employee);
	}

}
